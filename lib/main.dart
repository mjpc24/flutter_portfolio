//Packages imported.
import 'package:flutter/material.dart';
//Package imports created.
import '/utils/about_developer.dart';
void main() {
    runApp(AboutScreen());
}

class AboutScreen extends StatelessWidget{

    @override
    Widget build(BuildContext context) {
        
        return MaterialApp(
            
            home:Scaffold(
                appBar: AppBar(
                    title: Text('About the Developer'),
                    centerTitle: true,
                    backgroundColor:Color.fromRGBO(20, 45, 68, 1) ,
                ),
                body:SingleChildScrollView(
                    child: Container(
                        child:Column(
                            children: <Widget>[
                                Stack(
                                    // ignore: deprecated_member_use
                                    overflow: Overflow.visible,
                                    alignment: Alignment.center,
                                    children: [
                                        Image.asset('/assets/flutter.png',
                                        // height: MediaQuery.of(context).size.height/3,
                                        fit:BoxFit.cover),
                                        Positioned(
                                            bottom: -110.0,
                                            child: CircleAvatar(
                                                radius: 100,
                                                backgroundColor: Colors.white,
                                                backgroundImage: AssetImage('/assets/mjpc.jpg'),
                                            )
                                        )
                                    ],
                                ),
                                SizedBox(
                                    height: 120
                                ),devDetails,
                                SizedBox(
                                    height: 25
                                ),
                                aboutMe
                            ],
                        ),
                    )
                ),
            )
        );
    }
}


